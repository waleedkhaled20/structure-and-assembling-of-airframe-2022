If you have any trouble using GitLab and GitHub Disktop, check the steps below.

1) Download GitHub Desktop from google chrome, then after downloading it Sign in.
3) Copy the URL of our Project in GitLab, you will find it on the main page of the project, press on Clone, then Clone with HTTPS (copy the link)
4) Go to GitHub Desktop, click on clone Repositry from the internet, choose URL, paste the link you copied in step number 3, and for the local path choose the directory where you want to clone, and finally click to clone button.
5) You will see the current repository, then click on your current branch and choose which group you are, according to the team selection done by the teacher on teams.
6) Always be up to date and click on Fetch origin, to see all the new files and corrections in (GitHub Desktop).

Good luck and let me know if there is something to help with.
